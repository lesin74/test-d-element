const burgerMenu = document.querySelector('.burger__menu'),
      headerMenu = document.querySelector('.header__menu'),
      body = document.body,
      background = document.createElement('div'),
      closeMenu = document.querySelector('.header__close_menu'),
      openModal = document.querySelector('.open__modal'),
      modalWindow = document.createElement('div'),
      popup = document.createElement('div'),
      sendmsgSubmit = document.querySelectorAll('.sendmsg__submit'),
      sendmsgName = document.querySelector('.sendmsg__name'),
      sendmsgEmail = document.querySelector('.sendmsg__email'),
      sendmsgMessage = document.querySelector('.sendmsg__message'),
      labelName = document.querySelector('.label__name'),
      labelEmail = document.querySelector('.label__email'),
      labelMessage = document.querySelector('.label__message'),
      form = document.querySelector('.form');

function Menu(){
  burgerMenu.addEventListener('click', function(){
      body.appendChild(background);
      headerMenu.classList.toggle('header__active');
      background.classList.add('close__menu');
  });

  background.addEventListener('click', function(){
      background.remove();
      headerMenu.classList.toggle('header__active');
  })

  closeMenu.addEventListener('click', function(){
      background.remove();
      headerMenu.classList.toggle('header__active');
  })
}

function modalForm(){
    openModal.addEventListener('click', function(){
        body.style.overflow = 'hidden';
        body.style.marginRight = '16px';
        body.appendChild(modalWindow);
        modalWindow.classList.add('modal__window');
        modalWindow.innerHTML = `
        <div class="modal__container sendmsg__form">
            <form class="form form__modal" action="send.php" method="POST" onsubmit="return false;">
                <h3>Send Us Message</h3>
                <div class="sendmsg__inputs">
                    <label class="label__name label__name_modal">Full Name</label>
                    <input class="sendmsg__name sendmsg__name_modal" type="text" placeholder="Your Name" name="name">
                    <label class="label__email label__email_modal">Email</label>
                    <input class="sendmsg__email sendmsg__email_modal" type="email" placeholder="Your Email" name="email">
                    <label class="label__message label__message_modal">Message</label>
                    <textarea class="sendmsg__message sendmsg__message_modal" name="message" id="" cols="30" rows="10" placeholder="Your Message"></textarea>
                    <button class="sendmsg__submit sendmsg__submit_modal" type="submit">Submit</button>
                </div>
            </form>
        </div>
        `;
  
    const sendmsgSubmitModal = document.querySelector('.sendmsg__submit_modal');
  
    sendmsgSubmitModal.addEventListener('click', () => {
        isValidModal();
    })
})

    modalWindow.addEventListener('click', function(event){
    if(event.target == modalWindow){
        modalWindow.remove();
        body.style.overflow = 'auto';
        body.style.marginRight = '0';
    }
})
}

const isValid = () =>{
if(sendmsgName.value === '' || sendmsgEmail.value === '' || sendmsgMessage.value === '' || !isNaN(sendmsgName.value)){
    if(sendmsgName.value.trim() === ''){
        labelName.style.color = 'red';
        sendmsgName.style.border = 'solid red 1px';
        } else {
            labelName.style.color = 'green';
            sendmsgName.style.border = 'solid green 1px';
        }
    if(sendmsgEmail.value === ''){
        labelEmail.style.color = 'red';
        sendmsgEmail.style.border = 'solid red 1px';
        } else {
            labelEmail.style.color = 'green';
            sendmsgEmail.style.border = 'solid green 1px';
        }
    if(sendmsgMessage.value.trim() === ''){
        labelMessage.style.color = 'red';
        sendmsgMessage.style.border = 'solid red 1px';
        } else {
            labelMessage.style.color = 'green';
            sendmsgMessage.style.border = 'solid green 1px';
        }
    } else {
        labelName.style.color = 'green';
        labelEmail.style.color = 'green';
        labelMessage.style.color = 'green';
        sendmsgName.style.border = 'solid green 1px';
        sendmsgEmail.style.border = 'solid green 1px';
        sendmsgMessage.style.border = 'solid green 1px';
            popUpShow();
                setTimeout(function(){
                    popUpHide();
                }, 4000);
            setTimeout(function(){
                form.submit();
            }, 6000);
        }
}

const isValidModal = () =>{
    const labelNameModal = document.querySelector('.label__name_modal'),
          sendmsgNameModal = document.querySelector('.sendmsg__name_modal'),
          labelEmailModal = document.querySelector('.label__email_modal'),
          sendmsgEmailModal = document.querySelector('.sendmsg__email_modal'),
          labelMessageModal = document.querySelector('.label__message_modal'),
          sendmsgMessageModal = document.querySelector('.sendmsg__message_modal'),
          formModal = document.querySelector('.form__modal');

    if(sendmsgNameModal.value === '' || sendmsgEmailModal.value === '' || sendmsgMessageModal.value === '' || !isNaN(sendmsgNameModal.value)){
        if(sendmsgNameModal.value.trim() === ''){
            labelNameModal.style.color = 'red';
            sendmsgNameModal.style.border = 'solid red 1px';
            } else {
                labelNameModal.style.color = 'green';
                sendmsgNameModal.style.border = 'solid green 1px';
            }
        if(sendmsgEmailModal.value === ''){
            labelEmailModal.style.color = 'red';
            sendmsgEmailModal.style.border = 'solid red 1px';
            } else {
                labelEmailModal.style.color = 'green';
                sendmsgEmailModal.style.border = 'solid green 1px';
            }
        if(sendmsgMessageModal.value.trim() === ''){
            labelMessageModal.style.color = 'red';
            sendmsgMessageModal.style.border = 'solid red 1px';
            } else {
                labelMessageModal.style.color = 'green';
                sendmsgMessageModal.style.border = 'solid green 1px';
            }
        } else {
            labelNameModal.style.color = 'green';
            labelEmailModal.style.color = 'green';
            labelMessageModal.style.color = 'green';
            sendmsgNameModal.style.border = 'solid green 1px';
            sendmsgEmailModal.style.border = 'solid green 1px';
            sendmsgMessageModal.style.border = 'solid green 1px';
                popUpShow();
                setTimeout(function(){
                    popUpHide();
                }, 4000);
                setTimeout(function(){
                    formModal.submit();
                }, 6000);
            }
}

function submitForm(){
    sendmsgSubmit.forEach((el) => {
        el.addEventListener('click', function(){
            isValid();
        })
    })
}

const popUpShow = () =>{
        body.appendChild(popup);
        popup.classList.add('popup');
        popup.innerHTML = `
            <div class="popup__message">Your message successfully sent</div>
        `;
}

const popUpHide = () =>{
    popup.remove();

}

Menu();

submitForm();

modalForm();
